# Day 1 - Inventory Management System
# Given a list of box IDs, what is the checksum for your list of box IDs

from pathlib import Path
import string

def read_input_into_memory (file_path):
    # Open the file as read-only
    with open(file_path, "r") as input_file:
        input_str = input_file.read().strip()  # remove any whitespace from either end of string
        return input_str


INPUT_PATH = Path.cwd() / "day2_input.txt"
# INPUT_PATH = Path.cwd() / "day2_test_input.txt"
assert INPUT_PATH.exists()
box_ids = read_input_into_memory(INPUT_PATH)
box_ids = [x for x in box_ids.split("\n")]  # create a list of strings, new string each time is sees \n


# Part 1
# What is the checksum for your list of box IDs?

alphabet = string.ascii_lowercase[:26]

box_ids_counted = []
sum_of_threes = 0
sum_of_twos = 0
for box in box_ids:
    has_a_three = False
    has_a_two = False
    for letter in alphabet:
        if box.count(letter) == 3:
            has_a_three = True
        elif box.count(letter) == 2:
            has_a_two = True
    if has_a_three:
        sum_of_threes += 1
    if has_a_two:
        sum_of_twos += 1

checksum = sum_of_twos * sum_of_threes

print("Part 1, sum_of_twos * sum_of_threes: " + str(checksum))

# Part 2
# Looping through the frequency changes, what is the first duplicate frequency

magic_box_1 = ""
magic_box_2 = ""
solution = ""
for box in range(0, len(box_ids) - 1):  # Gives list index
    if magic_box_1 != "":
        break
    else:
        for comparison_box in range(box, len(box_ids) -1):
            # setup
            box_a = list(box_ids[box])
            box_b = list(box_ids[comparison_box])
            num_mismatched_characters = 0

            # iterate to find number of matching chars in this pair
            for character in range(0, len(box_a)):
                if box_a[character] != box_b[character]:
                    num_mismatched_characters += 1

            # Have found the pair if only 1 char different
            if num_mismatched_characters == 1:
                # have found the pair of boxes
                magic_box_1 = box_ids[box]
                magic_box_2 = box_ids[comparison_box]

                # build solution string
                for character in range(0, len(box_a)):
                    if box_a[character] == box_b[character]:
                        solution = solution + str(box_a[character])

                break

print("Magic Box 1: " + str(magic_box_1))
print("Magic Box 2: " + str(magic_box_2))
print("Solution code: " + str(solution))

# Part 1 Answer: 6175
# NOT 15875 - too high

# Part 2 Answer: asgwjcmzredihqoutcylvzinx
