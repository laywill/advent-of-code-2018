# Day 1 - Inventory Management System
# Given a list of box IDs, what is the checksum for your list of box IDs

from pathlib import Path


def read_input_into_memory(file_path):
    # Open the file as read-only
    with open(file_path, "r") as input_file:
        input_str = input_file.read().strip()  # remove any whitespace from either end of string
        print("File read complete.")
        return input_str


def process_input(input_string):
    claims_list = [x for x in input_string.split("\n")]  # create a list of strings, new string each time is sees \n
    for line in range(0, len(claims_list)):
        #
        # STRING PROCESSING
        #

        # create list of elements
        claims_list[line] = [x for x in claims_list[line].split(" ")]
        # remove redundant @ symbol
        del claims_list[line][1]
        # remove redundant # symbol on number and cast to int
        claims_list[line][0] = int(claims_list[line][0][1:])
        # split inset into strings X Y coordinates in list
        claims_list[line][1] = claims_list[line][1].split(",")
        # remove redundant : symbol on end of number
        claims_list[line][1][1] = claims_list[line][1][1][:-1]
        # split size into strings X Y dimensions in list
        claims_list[line][2] = claims_list[line][2].split("x")

        #
        # CONVERT TO INT
        #

        claims_list[line][1] = list(map(int, claims_list[line][1]))
        # claims[line][1][1] = int(claims[line][1][1])
        claims_list[line][2] = list(map(int, claims_list[line][2]))
        # claims[line][2][0] = int(claims[line][2][0])
        # claims[line][2][1] = int(claims[line][2][1])
    print("Input processed.")
    return claims_list


class Day3:

    def __init__(self):
        """Initialise"""

        # Empty
        self._playing_field = []

    def try_to_claim_next(self, claim_vector):
        """
        Check if the playing field is big enough to process the claim_vector.
        Extend the playing field if it isn't, filling new cells with 0.
        Increment the values in the cells the claim wants.
        Resulting cell values:
        - 0 == initialised, unclaimed.
        - 1 == claimed successfully by someone.
        - >1 == contested
        """
        # Vector format:
        # [Claim_Number, [Start_Col_Num, Start_Row_Num], [Num_Cols_Wide, Num_Rows_Tall]]
        start_col = claim_vector[1][0]
        start_row = claim_vector[1][1]
        num_cols_wide = claim_vector[2][0]
        num_rows_tall = claim_vector[2][1]

        print("Claim: " + str(claim_vector))

        # If required, extend the playing field column count with empty lists
        if len(self._playing_field) < max(len(self._playing_field),
                                          (start_col + num_cols_wide)
                                          ):
            for col in range(len(self._playing_field),
                             max(len(self._playing_field),
                                 (start_col + num_cols_wide))
                             ):
                self._playing_field.append([])

        # If required, extend the playing field row lists by filling lists with 0s
        for col in range(0, len(self._playing_field)):
            if len(self._playing_field[col]) < max(len(self._playing_field[0]),
                                                   (start_row + num_rows_tall)
                                                   ):
                for row in range(len(self._playing_field[col]),
                                 max(len(self._playing_field[0]),
                                     (start_row + num_rows_tall))
                                 ):
                    self._playing_field[col].append(0)

        print("New size, cols x rows: " + str(len(self._playing_field)) + " x " + str(len(self._playing_field[0])))

        # For the squares which are in the claim, increment the value in that square
        for col in range(start_col, (start_col + num_cols_wide)):
            for row in range(start_row, (start_row + num_rows_tall)):
                # print("Claim: " + str(claim_vector))
                # print("Filed size, cols x rows: " + str(len(self._playing_field)) + " x " + str(len(self._playing_field[0])))
                # print("Incrementing cell [Col x Row]:\n[" + str(col) + " x " + str(row) + "]")
                self._playing_field[col][row] += 1

    def how_many_conflicting_claims(self):
        """
        Find the area of the playing field for total number of squares available to claim.
        Subtract number of uncontested squares (all those unclaimed, and successfully claimed).
        Return the number of cells which are neither unclaimed or successfully claimed.
        """
        num_unclaimed = 0
        num_claimed = 0
        for col in range(0, len(self._playing_field)):
            num_unclaimed += self._playing_field[col].count(0)
            num_claimed += self._playing_field[col].count(1)

        playing_field_area = (len(self._playing_field)) * (len(self._playing_field[0]))

        num_conflicts = playing_field_area - num_claimed - num_unclaimed

        return num_conflicts

    def find_conflict_free_claim(self, claim_vector):
        # For the squares which are in the claim, are any of the cells > 1
        conflict = False
        start_col = claim_vector[1][0]
        start_row = claim_vector[1][1]
        num_cols_wide = claim_vector[2][0]
        num_rows_tall = claim_vector[2][1]

        for col in range(start_col, (start_col + num_cols_wide)):
            for row in range(start_row, (start_row + num_rows_tall)):
                # print("Claim: " + str(claim_vector))
                # print("Filed size, cols x rows: " + str(len(self._playing_field)) + " x " + str(len(self._playing_field[0])))
                # print("Incrementing cell [Col x Row]:\n[" + str(col) + " x " + str(row) + "]")
                if self._playing_field[col][row] > 1:
                    conflict = True

        return conflict


# This runs if this module is loaded directly by the interpreter
# If it is imported from another module, then only the class
# definitions above are run, and not the block in this if statement.
# This means that this class could potentially be reused elsewhere,
# without having side-effects when it is imported.
if __name__ == "__main__":
    DAY3 = Day3()
    INPUT_PATH = Path.cwd() / "day3_input.txt"
    # INPUT_PATH = Path.cwd() / "day3_test_input.txt"
    assert INPUT_PATH.exists()
    claims = read_input_into_memory(INPUT_PATH)
    claims = process_input(claims)

    for move in claims:
        DAY3.try_to_claim_next(move)

    num_squares_contested = DAY3.how_many_conflicting_claims()
    print("Number of contested squares: " + str(num_squares_contested))

    # PART 2
    for move in claims:
        does_this_move_conflict = DAY3.find_conflict_free_claim(move)
        if not does_this_move_conflict:
            print("The claim which does not conflict is: " + str(move))
            break
        else:
            print("CONFLICT @ Claim " + str(move))

# Part 1
# How many square inches of fabric are within two or more claims?
# Answer: 114946
# NOT 996003 - too high
# NOT  95607 - too low
# NOT  97604 - too low

# Part 2
# Which claim does not conflict with any other claim?
# Answer: 877
