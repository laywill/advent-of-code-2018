# Day 4 - Alchemical Reduction
# ou scan the chemical composition of the suit's material
#   and discover that it is formed by extremely long polymers
# The polymer is formed by smaller units which, when triggered,react with each other such
#   that two adjacent units of the same type and opposite polarity are destroyed.
#   Units' types are represented by letters; units' polarity is represented by capitalization.

from pathlib import Path


class Day5:

    def __init__(self):
        """Initialise"""

        # Empty
        self._input_str = ""
        self._polymer = []

    def read_input_into_memory(self, file_path):
        # Open the file as read-only
        with open(file_path, "r") as input_file:
            self._input_str = input_file.read().strip()  # remove any whitespace from either end of string
            print("File read complete.")
            # return input_str

    def process_input(self):
        print("Processing input!")
        self._polymer = [x for x in self._input_str]

    def react_pair(self, unit):
        print("Reacting two units of polymer components to reduce it's length!")
        # did_they_react = False
        if self._polymer[unit].lower() == self._polymer[unit + 1].lower():
            # units are the same character when Case converted
            if self._polymer[unit].isupper and self._polymer[unit + 1].islower:
                # units are the same character, first upper, second lower
                del self._polymer[unit]
                del self._polymer[unit]
            elif self._polymer[unit].islower == self._polymer[unit + 1].isupper:
                # units are the same character, first lower, second upper
                del self._polymer[unit]
                del self._polymer[unit]
        # else:
            # units are the same character and case
        # return did_they_react

    def react_polymer(self):
        print("Reacting the whole polymer, stand back!")
        go_again = False
        try:
            for unit in range(0, len(self._polymer) -1):
                self.react_pair(unit)
        except:
            go_again = False
        return go_again


    def part1_runner(self, file_path):
        print("Part 1 - Running!")
        self.read_input_into_memory(file_path)
        self.process_input()
        while True:
            keep_going = self.react_polymer()
            if not keep_going:
                break
        print("Length of final polymer: " + str(len(self._polymer)))


    def part2_runner(self):
        # self.any_guard_sleepiest_minute()
        print("Part 2 - Running!")


# This runs if this module is loaded directly by the interpreter
# If it is imported from another module, then only the class
# definitions above are run, and not the block in this if statement.
# This means that this class could potentially be reused elsewhere,
# without having side-effects when it is imported.
if __name__ == "__main__":
    DAY5 = Day5()
    INPUT_PATH = Path.cwd() / "day5_input.txt"
    # INPUT_PATH = Path.cwd() / "day5_test_input.txt"
    assert INPUT_PATH.exists()
    DAY5.part1_runner(INPUT_PATH)
    DAY5.part2_runner()

    print("DONE!")

# Part 1
# How many units remain after fully reacting the polymer you scanned?
# Answer: ?
# NOT 28258 - too high

# Part 2
# Of all guards, which guard is most frequently asleep on the same minute?
# Answer: 16164
# NOT 449 - too low, right guard, but didn't read the question.
