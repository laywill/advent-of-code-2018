# Day 1 - Chronal Calibration
# Starting Frequency 0
# Given a list of frequency shifts, what is final frequency

from pathlib import Path


def find_first_duplicate_freq(frequencies):
    # set is a datatype. Created from a list. Can only contain each element once.
    # Used here to de-duplicate list.
    # Comparing length of list against length of set tells you if anything was duplicated.
    return len(frequencies) != len(set(frequencies))


def read_input_into_memory (file_path):
    # Open the file as read-only
    with open(file_path, "r") as input_file:
        input_str = input_file.read().strip()  # remove any whitespace from either end of string
        return input_str


INPUT_PATH = Path.cwd() / "day1_input.txt"
# INPUT_PATH = Path.cwd() / "day1_test_input.txt"
assert INPUT_PATH.exists()
freq_changes = read_input_into_memory(INPUT_PATH)
freq_changes = [int(x) for x in freq_changes.split("\n")]  # create a list of strings, new string each time is sees \n

# Part 1
# what is the resulting frequency after all of the changes in frequency have been applied from 1 run through?

# Starting with a frequency of zero
pt1_total_shift = 0

for change in freq_changes:
    pt1_total_shift = pt1_total_shift + change

print("Part 1, Total shift from 1 iteration: " + str(pt1_total_shift))

# Part 2
# Looping through the frequency changes, what is the first duplicate frequency

# Starting with a frequency of zero
searching = True
pt2_total_shift = 0
frequencies_seen = []
while searching:
    for change in freq_changes:
        pt2_total_shift = pt2_total_shift + change
        frequencies_seen.append(pt2_total_shift)
        if find_first_duplicate_freq(frequencies_seen):
            print("Part 2, First duplicate frequency is: " + str(frequencies_seen[len(frequencies_seen) - 1]))
            searching = False
            break


# Part 1 Answer: 553

# Part 2 Answer: 78724
