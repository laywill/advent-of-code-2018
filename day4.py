# Day 4 - Repose Record
# You need to sneak inside and fix the issues with the suit, but there's a guard
#   stationed outside the lab, so this is as close as you can safely get.
# If you can figure out the guard most likely to be asleep at a specific time, you might be able
#   to trick that guard into working tonight so you can have the best chance of sneaking in.

from pathlib import Path


def takeFirst(elem):
    return elem[0]


class Day4:

    def __init__(self):
        """Initialise"""

        # Empty
        self._input_str = ""
        self._recorded_times = []
        self._guard_records = {}
        self._guard_totals = {}

    def read_input_into_memory(self, file_path):
        # Open the file as read-only
        with open(file_path, "r") as input_file:
            self._input_str = input_file.read().strip()  # remove any whitespace from either end of string
            print("File read complete.")
            # return input_str

    def process_input(self):
        self._recorded_times = [x for x in self._input_str.split("\n")]  # create a list of strings, new string each \n
        for line in range(0, len(self._recorded_times)):
            # create list of elements
            self._recorded_times[line] = self._recorded_times[line].split("]")
            # remove redundant [ symbol on Date
            self._recorded_times[line][0] = self._recorded_times[line][0][1:]
            # remove redundant whitespace on recorded action
            self._recorded_times[line][1] = self._recorded_times[line][1][1:]

            # Sort List
            self._recorded_times.sort(key=takeFirst)
        print("Input processed.")
        # return recorded_times

    def split_time_from_date(self):
        """ Split Date and Time in the recorded times"""
        for line in range(0, len(self._recorded_times)):
            self._recorded_times[line][0] = self._recorded_times[line][0].split(" ")

    def split_hours_from_minutes(self):
        """Split Hours and Minutes in the recorded times"""
        for line in range(0, len(self._recorded_times)):
            self._recorded_times[line][0][1] = self._recorded_times[line][0][1].split(":")
            self._recorded_times[line][0][1][0] = int(self._recorded_times[line][0][1][0])
            self._recorded_times[line][0][1][1] = int(self._recorded_times[line][0][1][1])

    def strip_guff_from_guard_number(self):
        """Remove all the guff in the sentence about which guard comes on duty. Leave just guard number"""
        for line in range(0, len(self._recorded_times)):
            # If it is a "Guard #X begins shift" statement:
            if self._recorded_times[line][1][0] == "G":
                # Strip "Guard #" from start of string
                self._recorded_times[line][1] = self._recorded_times[line][1][7:]
                # Strip " begins shift" from end of string
                self._recorded_times[line][1] = int(self._recorded_times[line][1][:-13])

    def fill_guard_records(self):
        """
        Take the contents of _recorded_times and convert it to data on
        when each guard sleeps in _guard_records
        """
        last_guard = 0
        sleep_start = 0
        for line in range(0, len(self._recorded_times)):
            # Check if it is an integer, or whether it is a string to start / stop sleeping
            if isinstance(self._recorded_times[line][1], int):
                # Create a record for the guard if they don't have one
                last_guard = self._recorded_times[line][1]
                if last_guard not in self._guard_records:
                    self._guard_records[last_guard] = [0] * 60
            else:  # It is a string.
                if self._recorded_times[line][1][0] == "f":
                    # Guard falling asleep, record sleep time (minutes only)
                    sleep_start = self._recorded_times[line][0][1][1]
                if self._recorded_times[line][1][0] == "w":
                    # Guard waking up, record wake time (minutes only)
                    sleep_end = self._recorded_times[line][0][1][1]
                    print("Guard #" + str(last_guard) + " slept from " + str(sleep_start) + " to " + str(sleep_end))
                    for minute_asleep in range(sleep_start, sleep_end):
                        self._guard_records[last_guard][minute_asleep] += 1

    def find_the_sleepiest_guard(self):
        for guard in self._guard_records:
            self._guard_totals[guard] = sum(self._guard_records[guard])
        sleepy_guard = max(zip(self._guard_totals.values(), self._guard_totals.keys()))
        print("The sleepiest guard is Guard number " + str(sleepy_guard[1]) + " who slept for " + str(
            sleepy_guard[0]) + " minutes.")
        return sleepy_guard[1]

    def sleepiest_guards_sleepiest_minute(self, sleepiest_guard):
        return self._guard_records[sleepiest_guard].index(max(self._guard_records[sleepiest_guard]))

    def any_guard_sleepiest_minute(self):
        sleepiest_minute = []
        for guard in self._guard_records:
            sleepiest_minute.append({"Guard": guard,
                                     "Minute": self._guard_records[guard].index(max(self._guard_records[guard])),
                                     "Occurrences": (max(self._guard_records[guard]))
                                     })
        overall_sleepiest = max(sleepiest_minute, key=lambda x: x['Occurrences'])
        print("Sleepiest Guard: " + str(overall_sleepiest["Guard"]))
        print("Sleepiest Minute: " + str(overall_sleepiest["Minute"]))
        print("Solution: " + str(overall_sleepiest["Guard"] * overall_sleepiest["Minute"]))

    def part1_runner(self, file_path):
        self.read_input_into_memory(file_path)
        self.process_input()
        self.split_time_from_date()
        self.split_hours_from_minutes()
        self.strip_guff_from_guard_number()
        self.fill_guard_records()
        sleepy_guard = self.find_the_sleepiest_guard()
        sleepy_minute = self.sleepiest_guards_sleepiest_minute(sleepy_guard)
        print("Sleepy Guard: " + str(sleepy_guard))
        print("Sleepy Minute: " + str(sleepy_minute))
        print("Sleepy Guard * Sleepy Minute = " + str(sleepy_guard * sleepy_minute))

    def part2_runner(self):
        self.any_guard_sleepiest_minute()


# This runs if this module is loaded directly by the interpreter
# If it is imported from another module, then only the class
# definitions above are run, and not the block in this if statement.
# This means that this class could potentially be reused elsewhere,
# without having side-effects when it is imported.
if __name__ == "__main__":
    DAY4 = Day4()
    INPUT_PATH = Path.cwd() / "day4_input.txt"
    # INPUT_PATH = Path.cwd() / "day4_test_input.txt"
    assert INPUT_PATH.exists()
    DAY4.part1_runner(INPUT_PATH)
    DAY4.part2_runner()

    print("DONE!")

# Part 1
# the Sleepiest Guard's number * the sleepiest Guard's Sleepiest Minute
# Answer: 12169

# Part 2
# Of all guards, which guard is most frequently asleep on the same minute?
# Answer: 16164
# NOT 449 - too low, right guard, but didn't read the question.
